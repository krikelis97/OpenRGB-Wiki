# Genesis Thor 300

 Direct mode is not supported by the keyboard

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `258A` | `0090` | Genesis Thor 300 |
