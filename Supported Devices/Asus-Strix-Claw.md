# Asus Strix Claw

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are not fully implemented by controller (See device page for details)

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `195D` | `1016` | ASUS ROG Strix Claw |
