# AsusAuraCoreLaptop

 Power profiles for this controller are set to `On` for all power
state and scan be adjusted in the JSON config file.

For each zone available LEDs can be set as `On = true` or `Off = false` when
* Booting
* Awake (Normal Usage)
* Sleeping
* Shutdown / Power Off

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `19B6` | ASUS ROG Strix SCAR 15 |
