# SRGBmods Raspberry Pi Pico LED Controller

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `16D0` | `1123` | SRGBmods Pico LED Controller |
