# Wooting Keyboards

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `03EB` | `FF01` | Wooting One (Legacy) |
| `03EB` | `FF02` | Wooting Two (Legacy) |
| `31E3` | `1100` | Wooting One (Xbox) |
| `31E3` | `1101` | Wooting One (Classic) |
| `31E3` | `1102` | Wooting One (None) |
| `31E3` | `1200` | Wooting Two (Xbox) |
| `31E3` | `1201` | Wooting Two (Classic) |
| `31E3` | `1202` | Wooting Two (None) |
| `31E3` | `1210` | Wooting Two LE (Xbox) |
| `31E3` | `1211` | Wooting Two LE (Classic) |
| `31E3` | `1212` | Wooting Two LE (None) |
| `31E3` | `1220` | Wooting Two HE (Xbox) |
| `31E3` | `1221` | Wooting Two HE (Classic) |
| `31E3` | `1222` | Wooting Two HE (None) |
| `31E3` | `1300` | Wooting Two 60HE (Xbox) |
| `31E3` | `1301` | Wooting Two 60HE (Classic) |
| `31E3` | `1302` | Wooting Two 60HE (None) |
