# Asus Aura Headset Stand

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `18D9` | ASUS ROG Throne |
| `0B05` | `18C5` | ASUS ROG Throne QI |
| `0B05` | `1994` | ASUS ROG Throne QI GUNDAM |
