# Sony Dual Shock 4 controller

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `054C` | `05C4` | Sony DualShock 4 |
| `054C` | `09CC` | Sony DualShock 4 |
| `054C` | `0BA0` | Sony DualShock 4 |
