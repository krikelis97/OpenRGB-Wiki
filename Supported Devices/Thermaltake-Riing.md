# Thermaltake Riing

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `264A` | `1FA5` | Thermaltake Riing (PID 0x1FA5) |
| `264A` | `1FA6` | Thermaltake Riing (PID 0x1FA6) |
| `264A` | `1FA7` | Thermaltake Riing (PID 0x1FA7) |
| `264A` | `1FA8` | Thermaltake Riing (PID 0x1FA8) |
| `264A` | `1FA9` | Thermaltake Riing (PID 0x1FA9) |
| `264A` | `1FAA` | Thermaltake Riing (PID 0x1FAA) |
| `264A` | `1FAB` | Thermaltake Riing (PID 0x1FAB) |
| `264A` | `1FAC` | Thermaltake Riing (PID 0x1FAC) |
| `264A` | `1FAD` | Thermaltake Riing (PID 0x1FAD) |
| `264A` | `1FAE` | Thermaltake Riing (PID 0x1FAE) |
| `264A` | `1FAF` | Thermaltake Riing (PID 0x1FAF) |
| `264A` | `1FB0` | Thermaltake Riing (PID 0x1FB0) |
| `264A` | `1FB1` | Thermaltake Riing (PID 0x1FB1) |
| `264A` | `1FB2` | Thermaltake Riing (PID 0x1FB2) |
| `264A` | `1FB3` | Thermaltake Riing (PID 0x1FB3) |
| `264A` | `1FB4` | Thermaltake Riing (PID 0x1FB4) |
| `264A` | `1FB5` | Thermaltake Riing (PID 0x1FB5) |
