# Sapphire Nitro Glow v1

 

## Connection Type
 I2C

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor &<br/>Device ID | Sub-Vendor &<br/>Product ID | Device Name |
| :---: | :---: | :--- |
| `1002:67DF` | `174B:E347` | Sapphire RX 470/480 Nitro+ |
| `1002:67DF` | `1DA2:E366` | Sapphire RX 570/580/590 Nitro+ |
| `1002:67DF` | `1DA2:E399` | Sapphire RX 570/580/590 Nitro+ |
| `1002:6FDF` | `1DA2:E366` | Sapphire RX 580 Nitro+ (2048SP) |
| `1002:687F` | `1DA2:E37F` | Sapphire RX Vega 56/64 Nitro+ |
