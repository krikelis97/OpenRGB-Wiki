# Coolermaster Mouse Pad

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `2516` | `0109` | Cooler Master MP750 XL |
| `2516` | `0107` | Cooler Master MP750 Large |
| `2516` | `0105` | Cooler Master MP750 Medium |
