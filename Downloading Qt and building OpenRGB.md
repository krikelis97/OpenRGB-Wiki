# Getting set up for building on windows

[[_TOC_]]

[![Build OpenRGB on Windows](http://img.youtube.com/vi/dU6cSAAb9yg/0.jpg)](http://www.youtube.com/watch?v=dU6cSAAb9yg "Build OpenRGB on Windows")

## The Process

Somethings must be done in order. Others don't matter.

MSVC *must* be installed before installing Qt

However git can be installed at any point during the process without affecting anything

## MSVC

start by going [here](https://visualstudio.microsoft.com/thank-you-for-downloading-visual-studio-for-cplusplus/?sku=Community)

This will download the installer

After running the installer go and select the ``Desktop Developement With C++`` kit

Uncheck the following:

1. C++ Profiling
2. C++ ATL
3. Test Adapter for boost
4. Test Adapter for google
5. Live Share
6. C++ addr sanitizer

It should look something like

![this](Building_OpenRGB_on_windows/MSVC_Setup/MSVC_items.png)

Wait for that to install and then move on to the Qt section of the guide

## Qt

1. Method 1

   1. If for any reason the following installation link is broken and does not work please proceed to method 2

   2. `https://www.qt.io/download-thank-you`

2. Method 2

   1. Go [here](https://www.qt.io/download) and scroll down to the bottom until you find the ``Downloads for Open Source Users`` section

   2. Then click the ``Go Open Source`` button

   3. Now scroll to the bottom of that page as well and click ``Download the Qt Online Installer``

   4. After clicking on that you can click the download button on the page you were redirected to (Qt's website is really dumb -_-)

Finally the installer will download

Run the installer and choose custom installation (Or you *will* end up with a lot of garbage)

One thing to note is that you *will* need an account for this

When it lets you choose what you want then you need to choose the following:

* Qt

  * Qt 5.15.x (The latest at the time of writing is 5.15.2 but that may change)

    1. MSVC 2019 64-bit

    2. MSVC 2019 32-bit (Only if you want to test 32 bit or run a 32 bit system)

**DO NOT** select the entire Qt kit or Qt 5.15.x kit. You only need the 2 items prefaced by numbers above

When you are done it should look like

![this](Building_OpenRGB_on_windows/Qt_Setup/Only_Check_This.png)

## Installing Git

Head over to [Git's website](https://git-scm.com) and download git with the button to the bottom right

After running the installer it will let you pick what you want.

Uncheck the windows explorer stuff so it looks like

![this](Building_OpenRGB_on_windows/Git_Setup/Uncheck_WExplorer.png)

Now you can hit next until you get to the part where you choose your default text editor.

I normally use VSCode but it is up to you to choose what you want

Leave everything else default until you get to "Configuring the terminal emulator to use with git bash"

Make sure you check "Use Windows' default console window"

![Default terminal](Building_OpenRGB_on_windows/Git_Setup/Default_Terminal.png)

Everything else can be left at default

## Downloading Source

After git is installed you can open a terminal window (cmd in windows search) and run ``git clone https://gitlab.com/CalcProgrammer1/OpenRGB.git`` in the folder you want to download source to (You may need to cd into the folder. But that is out of the scope of this tutorial)

Now you can either double click the .pro file in the new folder created by git clone. Or you can click "Open project" in Qt creator

For plugin developement you will need to run OpenRGB at least once and create the ``plugins`` folder in ``appdata/roaming/OpenRGB``

This guide will use my (CoffeeIsLife's) plugins

Same git clone process as before ``git clone https://gitlab.com/herosilas12/OpenRGB-Plugins.git``

Now cd into the folder that was created with ``cd OpenRGB-Plugins`` and run git checkout for the branch you want ``git checkout Effects``

After building the plugin you will need to copy the DLL file created to the plugins folder we made earlier before the changes will apply in OpenRGB.

## Notes

If you run into any issues then a video guide is available [here](https://www.youtube.com/watch?v=dU6cSAAb9yg)

Happy coding :)
